package Quest3;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class Arvore {

	private No raiz;
	private Stack<Integer> pilha = new Stack<>();
	private Queue<Integer> fila = new LinkedList<>();

	public Arvore() {
		raiz = null;
	}

	public void inserir(int val) {
		No novo = new No();
		novo.item = val;
		novo.direita = null;
		novo.esquerda = null;

		if (raiz == null) {
			raiz = novo;
		} else {
			No atual = raiz;
			No anterior;
			while (true) {
				anterior = atual;
				if (val <= atual.item) {
					atual = atual.esquerda;
					if (atual == null) {
						anterior.esquerda = novo;
						return;
					}

				} else if (val > atual.item) {
					atual = atual.direita;
					if (atual == null) {
						anterior.direita = novo;
						return;
					}
				}
			}
		}
	}

	public void inOrder(No atual) {
		if (atual != null) {
			inOrder(atual.esquerda);
			System.out.print(atual.item + " ");
			inOrder(atual.direita);
		}
	}

	public void AndandoEmOrdem() {
		System.out.print("\n Exibindo em ordem: ");
		inOrder(raiz);
		System.out.println("");
	}

	private void consultarPares(No aux) {
		if (aux != null) {
			if (aux.item % 2 == 0) {
				pilha.add(aux.item);
			}
			consultarPares(aux.esquerda);
			consultarPares(aux.direita);
		}
	}
	}
