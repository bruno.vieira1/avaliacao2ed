package Quest2;

public abstract class Musica {
	private String nome;

	public Musica() {

	}

	public Musica(String nome) {
		super();
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "musica [nome=" + nome + "]";
	}

}
