package Quest2;

public class MusicaInternacional extends Musica {
	String nome;

	public MusicaInternacional() {

	}

	public MusicaInternacional(String nome) {
		super();
		this.nome = nome;

	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "MusicaInternacional [nome=" + nome + "]";
	}

}
