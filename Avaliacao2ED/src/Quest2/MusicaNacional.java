package Quest2;

public class MusicaNacional extends Musica {
	String nome;

	public MusicaNacional() {

	}

	public MusicaNacional(String nome) {
		super();
		this.nome = nome;

	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "MusicaNacional [nome=" + nome + "]";
	}

}
