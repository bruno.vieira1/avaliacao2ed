package Quest2;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;

public class Fila {
	static Scanner sc = new Scanner(System.in);
	private static List<Musica> fila = new LinkedList<>();
	private static Queue<MusicaNacional> n = new LinkedList<>();
	private static Stack<MusicaInternacional> i = new Stack<>();

	public static void main(String[] args) {

		operacoes(fila);
	}

	public static void operacoes(List<Musica> fila) {

		Integer op = 0;
		fila.add(new MusicaNacional("musica1"));
		fila.add(new MusicaNacional("musica2"));
		fila.add(new MusicaNacional("musica3"));
		fila.add(new MusicaNacional("musica4"));
		fila.add(new MusicaNacional("musica5"));
		fila.add(new MusicaInternacional("musica6"));
		fila.add(new MusicaInternacional("musica7"));
		fila.add(new MusicaInternacional("musica8"));
		fila.add(new MusicaInternacional("musica9"));
		fila.add(new MusicaInternacional("musica10"));
		fila.add(new MusicaInternacional("musica11"));
		organizar();

		do {
			System.out.println(
					"Informe qual operao deseja fazer:\n1=Listar a fila nacional n\n2=Listar a Pilha internacional i\n3=Sair do programa.");
			op = sc.nextInt();
			sc.nextLine();
			switch (op) {
			case 1: {
				for (MusicaNacional musica : n) {
					System.out.println(musica);
				}
				break;
			}
			case 2: {
				for (MusicaInternacional musica : i) {
					System.out.println(musica);
				}

				break;
			}case 3: {

				System.out.println("Voc� saiu da playlist.");
				System.exit(0);

				break;
			}
			default:
			}
		} while (true);
	}

//	public static MusicaNacional addMusicaNacional() {
//		System.out.print("Informe o nome da musica: ");
//		String nome = sc.nextLine();
//		fila.add(addMusicaNacional());
//		return new MusicaNacional(nome);
//	}

//	public static MusicaInternacional addMusicaInternacional() {
//		System.out.print("Informe o nome da musica: ");
//		String nome = sc.nextLine();
//		fila.add(addMusicaInternacional());
//		return new MusicaInternacional(nome);
//	}

	public static void organizar() {
		for (Musica musica : fila) {
			if (musica instanceof MusicaNacional) {
				n.add((MusicaNacional) musica);

			} else if (musica instanceof MusicaInternacional) {
				i.add((MusicaInternacional) musica);

			}
		}
	}
}
