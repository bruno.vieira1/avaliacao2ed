package Quest1;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Fila {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		Queue<Contato> fila = new LinkedList<>();
		operacoes(fila);
	}

	public static void operacoes(Queue<Contato> fila) {

		Integer op = 0;
		do {
			System.out.println(
					"Informe qual operao deseja fazer: 1=Inserir Entrevistado, 2=Pr�ximo Entrevistado, 4=sair");
			op = sc.nextInt();
			sc.nextLine();
			switch (op) {
			case 1: {
				fila.add(addContato());
				System.out.println("Musica adicionada com sucesso.");
				break;
			}
			case 2: {

				break;
			}
			case 3: {
				try {
					fila.poll();
					System.out.println("O primeiro contato foi exclu�do.");
				} catch (Exception e) {
					System.out.println("A fila est� vazia.");
				}
				break;
			}
			case 4: {
				System.out.println("Obrigado por utilizar os nossos servi�os :D.");
				System.exit(0);

				break;
			}
			default:
			}
		} while (true);
	}

	public static Contato addContato() {
		System.out.print("Informe o nome do contato: ");
		String nome = sc.nextLine();
		System.out.println("Informe o numero do telefone do contato: ");
		String numero = sc.nextLine();

		return new Contato(nome, numero);
	}

}
